### 项目启动

* 基础构建
```
1 确定安装了 nodejs & yarn
    yarn -v / yarn --version
    nodejs -v / nodejs --version
2 进入项目根目录
    cd *
3 安装所有依赖
    yarn install
```

* 启动服务
```
yarn run build
nohup yarn serve -l 3000 -s build &
```

* 备注
```
```