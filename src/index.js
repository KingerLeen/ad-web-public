import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';


class App extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            title:"贷款",
            tel:"4000-8888888",
            promise:"所有贷款申请在未成功前绝不会收取任何费用，为保护您的资金安全，请不要相信任何要求您支付费用的信息、邮件、电话等不实信息。",
            company:"都江堰市益鑫小额贷款有限公司",
            record:"蜀ICP备17041513号-1",
            note:"贷款有风险，出借需谨慎",
            description:"贷款，车贷，快速申请，钱贷走，车照开，24小时服务，放款快速",
            success:"我们将立刻安排受理！请静候我们为您来电~（电话咨询请拨打4000-8888888）",


            isSucc:true,

            money:"",
            name:"",
            car:"",
            address:"",
            phone:"",

            showErr:"none",massageErr:"",
        }
        this.moneyChange = this.moneyChange.bind(this);
        this.nameChange = this.nameChange.bind(this);
        this.carChange = this.carChange.bind(this);
        this.addressChange = this.addressChange.bind(this);
        this.phoneChange = this.phoneChange.bind(this);


        this.postInfo = this.postInfo.bind(this);
        this.setSucc = this.setSucc.bind(this);
        this.controlErr = this.controlErr.bind(this);
        this.checkPhone = this.checkPhone.bind(this);
    }

    moneyChange(event){
        this.setState({money: event.target.value});
    }
    nameChange(event){
        this.setState({name: event.target.value});
    }
    carChange(event){
        this.setState({car: event.target.value});
    }
    addressChange(event){
        this.setState({address: event.target.value});
    }
    phoneChange(event){
        this.setState({phone: event.target.value});
    }

    postInfo(){
        if(this.state.showErr===""||this.state.showSucc===""){
            return;
        }
        console.log(this.state);
        if(this.state.money===""){
            this.controlErr(true,"请填写您的贷款金额");
        }else if(this.state.name===""){
            this.controlErr(true,"请填写您的姓名");
        }else if(this.state.car==="请选择车辆信息"||this.state.car===""){
            this.controlErr(true,"请选择您的车辆信息");
        }else if(this.state.address===""){
            this.controlErr(true,"请填写您的地址");
        }else if(!this.checkPhone(this.state.phone)){
            this.controlErr(true,"请正确填写您的手机号码");
        }else{
            var setSucc_ = this.setSucc;
            axios({
                method:"POST",
                headers:{'Content-type':'application/json',},
                url:"http://47.108.26.30:8000/apis/postUser",
                data:{
                    "loan_amount":this.state.money,
                    "name":this.state.name,
                    "car_info":this.state.car,
                    "city":this.state.address,
                    "phone_number":this.state.phone,
                    "submit_url":window.location.href,
                },
            }).then(function(res){
                console.log(res);
                setSucc_(false);
            }).catch(function(error){
                console.log(error);
            });
        }
    }
    checkPhone(str){
        str = String(str).replace(/\s*/g,'');
        if(!/^1[3456789]\d{9}$/.test(str)){
            return false;
        }
        return true;
    }

    controlErr(isShow,massage){
        if(isShow){
            this.setState({showErr: "",massageErr:massage});
        }else{
            this.setState({showErr: "none"});
        }
    }
    setSucc(is){
        this.setState({
            isSucc:is
        });
    }
    componentDidMount(){
        var s = document.querySelector("#jsonp");
        s&&s.parentNode.removeChild(s);
        var script = document.createElement("script");
        script.id = "jsonp";
        script.src = 'meiqia.js';
        document.body.appendChild(script);

        script.src = 'jq.js';
        document.body.appendChild(script);
        script.src = 'layui.js';
        document.body.appendChild(script);


        const _this = this;
        axios.get("http://47.108.26.30:8000/apis/getAll").then(function (response) {
            console.log(response);
            _this.setState({
                title:response.data.data.title,
                tel:response.data.data.tel,
                promise:response.data.data.promise,
                company:response.data.data.company,
                record:response.data.data.record,
                note:response.data.data.note,
                description:response.data.data.description,
                success:response.data.data.success,
            });
        }).catch(function (error) { console.log(error); });
    }
        

    
    render(){
        return(
            <React.Fragment>
<head>
<meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
<meta charSet="utf-8"/>
<title>{this.state.title}</title>
<meta name="description" content={this.state.description} />
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
<meta name="renderer" content="webkit"/>
<meta httpEquiv="Cache-Control" content="no-siteapp"/>
<meta name="imagemode" content="force"/>
<meta name="apple-mobile-web-app-capable" content="yes"/>
<meta name="apple-mobile-web-app-status-bar-style" content="black"/>
<meta name="format-detection" content="telephone=no"/>
<meta name="keywords" content=""/>
<meta name="description" content=""/>
<meta name="author" content=""/>
<meta httpEquiv="Pragma" content="no-cache"/>
<meta httpEquiv="Expires" content="-1"/>
<link rel="icon" type="image/x-ico" href="../imgs/icon.ico" />

<link rel="stylesheet" href="../btrp.css"/>
<link rel="stylesheet" href="../bstrpm.css"/>

<link rel="stylesheet" href="../mui.picker.all.css"/>
<link rel="stylesheet" href="../style.css"/>
<link rel="stylesheet" href="../layui.css"/>
<link rel="stylesheet" href="../main.css"/>


</head>
<body style={{backgroundColor:"black"}}>

{this.state.isSucc?<React.Fragment>



<img src="../imgs/head.png" />

<br/>


<div className="tab-pane" id="location">
<div  >
    <div className="col-sm-12" >
        <center><h1 style={{fontWeight:"bold",color:"white"}} className="info-text">填写申请信息</h1></center>
    </div>

    <div className="col-sm-5 col-sm-offset-1">
        <div className="form-group label-floating">
        
            <div className="input-group">
                <img className="form-control" style={{width:"50px",height:"40px"}}  src="../imgs/m1.png"/>
                <input value={this.state.money} onChange={this.moneyChange} style={{height:"40px",width:"calc(100% - 50px)",textAlign:"center"}} type="number" min="0.00"  max="5000000.00" step="0.01" className="form-control" placeholder="请输入您的贷款金额"/>
                <span className="input-group-addon">万元</span>
            </div>
        </div>
    </div>

    <div className="col-sm-5">
        <div className="form-group label-floating">
            <div className="input-group">
                <img className="form-control" style={{width:"50px",height:"40px"}}  src="../imgs/m2.png"/>
                <input value={this.state.name} onChange={this.nameChange} style={{height:"40px",width:"calc(100% - 50px)",textAlign:"center"}} type="text" className="form-control" placeholder="请输入您的姓名"/>
                <span className="input-group-addon"></span>
            </div>

        </div>
    </div>
</div>


<div  >
    <div className="col-sm-5 col-sm-offset-1">
        <div className="form-group label-floating">

            <div className="input-group">
                <img className="form-control" style={{width:"50px",height:"40px"}}  src="../imgs/m3.png"/>

                <select value={this.state.car} onChange={this.carChange} style={{height:"40px",width:"calc(100% - 50px)",textAlign:"center",textAlignLast:"center"}} className="form-control">
                    <option>请选择车辆信息</option>
                    <option>名下无车</option>
                    <option>名下全款车</option>
                    <option>名下按揭车</option>
                    <option>名下按揭还清</option>
                </select>

                <span className="input-group-addon"></span>
            </div>


        </div>
    </div>

    <div className="col-sm-5">
        <div className="form-group label-floating">

            <div className="input-group">
                <img className="form-control" style={{width:"50px",height:"40px"}}  src="../imgs/m4.png"/>
                <input value={this.state.address} onChange={this.addressChange} style={{height:"40px",width:"calc(100% - 50px)",textAlign:"center"}} type="text" className="form-control" placeholder="请输入您的地址(例如:四川省 成都市 高新西区)"/>
                <span className="input-group-addon"></span>
            </div>

            {/* <div className="input-group">
                <img className="form-control" style={{width:"50px",height:"40px"}}  src="../14.png"/>

                <select style={{height:"40px",width:"calc(27%)",textAlign:"center",textAlignLast:"center"}} className="form-control">
                    <option disabled="" selected="">省</option>
                    <option>名下无车</option>
                    <option>名下全款车</option>
                    <option>名下按揭车</option>
                    <option>名下按揭还清</option>
                </select>
                <select style={{height:"40px",width:"calc(27%)",textAlign:"center",textAlignLast:"center"}} className="form-control">
                    <option disabled="" selected="">市</option>
                    <option>名下无车</option>
                    <option>名下全款车</option>
                    <option>名下按揭车</option>
                    <option>名下按揭还清</option>
                </select>
                <select style={{height:"40px",width:"calc(46% - 50px)",textAlign:"center",textAlignLast:"center"}} className="form-control">
                    <option disabled="" selected="">区</option>
                    <option>名下无车</option>
                    <option>名下全款车</option>
                    <option>名下按揭车</option>
                    <option>名下按揭还清</option>
                </select>
                
                <span className="input-group-addon"></span>
            </div> */}


        </div>
    </div>
</div>


<div  >
    <div className="col-sm-5 col-sm-offset-1">
        <div className="form-group label-floating">
            <div className="input-group">
                <img className="form-control" style={{width:"50px",height:"40px"}}  src="../imgs/m5.png"/>
                <input  value={this.state.phone} onChange={this.phoneChange} style={{height:"40px",width:"calc(100% - 50px)",textAlign:"center"}} type="number" className="form-control" placeholder="请输入您的11位手机号码"/>
                <span className="input-group-addon"></span>
            </div>
        </div>
    </div>

    <div className="col-sm-5">
        <div >
            <div style={{height:"20px",width:"100%"}} ></div>
            <button onClick={this.postInfo} style={{border:"none",color:"white",backgroundColor:"#012B72",height:"50px",width:"100%",textAlign:"center"}} type="submit" className="form-control">立即申请</button>
            <div style={{height:"30px",width:"100%"}} ></div>
        </div>
    </div>
</div>
</div>
    <div className="imgs">
        <img src="../imgs/1.png" />
        <img src="../imgs/2.png" />
        <img src="../imgs/3.png" />
        <img src="../imgs/4.png" />
    </div>

<div className="foot">
    
    <p style={{fontSize:"15px"}}>
        <strong style={{color:"#7f7f7f"}}>郑重承诺：</strong><br/>{this.state.promise}
    </p>
    <p>
        {this.state.company}<br/> {this.state.record}
    </p>
</div>

<br/><br/>

<div style={{width:"80%",height:"40%",backgroundColor:"#930000",position:"fixed",top:"25%",left:"10%",zIndex:9999,borderRadius:"20px",overflow:"hidden",display:this.state.showErr}}>
    <article style={{width:"80%",height:"35px",textAlign:"center",color:"white",position:"absolute",top:"20%",left:"10%"}}>
    <h1 style={{fontWeight:"bold"}} className="info-text">注意！</h1>
    <span>{this.state.massageErr}</span>
    </article>

    <button onClick={()=>this.controlErr(false)} style={{border:"none",color:"white",backgroundColor:"#750000",height:"20%",width:"100%",textAlign:"center",position:"absolute",bottom:"0%",borderRadius:"20px"}}>确 定</button>
</div>
</React.Fragment>:<React.Fragment>
<center><img style={{width:"70%"}} src="../imgs/success.jpg" /></center>
</React.Fragment>}


<section className="footer_btn" style={{height:"100px"}}>
<article className="footer_l" >
<p style={{width:"99.6%",height:"35px",backgroundColor:"#5A0000",textAlign:"center",color:"white"}}><span>{this.state.note}</span></p>
<a style={{backgroundColor:"#31004C"}} href="https://e-96345.chatnow.meiqia.com/dist/standalone.html"><img src="../imgs/chat_icon.png" style={{width:"10%",height:"35px"}}/> 在线咨询 <i>2</i></a>
<a style={{backgroundColor:"#7F3801"}}  href={"tel://"+this.state.tel}><img src="../imgs/tel_icon.png" style={{width:"10%",height:"35px"}}/>立刻拨打</a>
</article>
</section>

</body>
            
            </React.Fragment>
        );
    }
}





ReactDOM.render(
<App/>
    ,document.getElementById('root')
);